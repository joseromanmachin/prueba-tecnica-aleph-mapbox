# Prueba Tecnica Aleph Mapbox

## Descripcion 
- Desarrollar una aplicación en el que se implemente la librería de mapbox, y al pulsar en cualquier parte de la
pantalla se coloquen puntos en el mapa, dado la lista de puntos obtener sus imágenes, para ello se debe usar el sdk
de mapbox y llamados a un API.

- 1.- Desarrollar una aplicación en el que se implemente la librería de mapbox, y al pulsar en cualquier parte de la
    pantalla se coloquen puntos en el mapa, dado la lista de puntos obtener sus imágenes, para ello se debe usar el sdk
    de mapbox y llamados a un API.
- 2.- Una vez que se tenga los points en el mapa, mostrar en otra pantalla en forma de lista las imágenes
    correspondientes de esa área, dejo imagen de ejemplo, para ello te comparto el endpoint que se debe
    consumir para obtener dicha información (Anexo 1)
## Tecnologias & Tecnicas Empleadas

- Kotlin
- Mapbox
- MVVM
- Singleton
- Room 
- Binding
- Retrofit
- Glide

## Screenshot
<p align="center">
  <img src="https://dm2306files.storage.live.com/y4mzJ13aIgy-Ylan2Brl49DaeMgLADBl7wrxsfjzBiDPjuKyPm0lRWBZlYQiMJ0pB203FDPycBiMmKMCpz4pp2NISG3eLQ0_Ao0Zk0fqv_j_vPuGi2YULJ4cqlS3GG_oex4HEsEvG0O4wi-bh3_F7oTHgGT8h_Xgk50qWgLI6Nsaw0fHfL6FqolDaLn6zXBC2q-?width=297&height=660&cropmode=none width="297" height="660""/>

  <img src="https://dm2306files.storage.live.com/y4mhQqlsdUzad0hzYBiZvw1PLFVcoo_G2NGfTFTlqgKESgmfR3rH4bzPTWh7_XaAQbmdZGZZ0QaUe4RPBXBLUPBWQvFDdbM4WW-_XrpYuiGKuDMPMtRmTua0Lw9MbeUooEdSnOZ2hetSnIINFXiLgJxZWetZSKmmirvihALeyMLKFJHu9vUqhUC9Faaa0AO6QZa?width=297&height=660&cropmode=none" width="297" height="660">

  <img src="https://dm2306files.storage.live.com/y4mGzRDOEgm3pfhkjrVXGrk_knSlpHP5YZ-dQar1p7Kk_t4GmfM0KzAgOzBq84_NeejkSprOst8VDDsWRTwQV2CSYO4nGt_lPXyXCKN4db63p2rwvlxsFnZ33hKL10a2bKAEmhLzyfyjOeV1EQe-rsbSXts_eoLkEit_Xvqk89SsCzALGTooWqWnwhjN_syJ9LD?width=297&height=660&cropmode=none" width="297" height="660" />
  
  <img src="https://dm2306files.storage.live.com/y4mc4h4PdamI8Aazqx_LTJB-PhYxJeiLl4aBxIHsRyn_GDKP7gJX1L3pAP3npxfwoIYasYgk0UD_j38FQ_LqVjEzRKJOU5wfNhl_bRheB3dWkGh9qEX76oIZrO3GpwX0gzq4PssGmjHTNo5YB3L53BLUxDu6l-RYU_7WXhJdv9zYQQ_5dwVr9AZXUWoDiAbtUKi?width=297&height=660&cropmode=none" width="297" height="660" />

  <img src="https://dm2306files.storage.live.com/y4m3SujNNbMe0G5qFgGayyHLkF6hNQ8r2vG8_XtUdxHzURwH3OkcNMJO-FDv5AU1oQ_CxTrK9dAFF5j6i1Sl0BueCmZN69RJzCOpIRgyJoPkdVOl14wkpgGYhiHEC8QEYqg9QQf0xRhg_SQDROQtGEwtjjgFnsPXTfwzGk-petYNpxpMNH34ZkrPi1c87UqdgFx?width=297&height=660&cropmode=none" width="297" height="660" />

  <img src="https://dm2306files.storage.live.com/y4mOQP3RWIn16GzOBNHoUFpOWuMP98NHulmLyPsVmSZhd4FU_IJxZibI_pQkYwpqNY8QpoF4D9Z1UTrqhTVa_-XFD1U597owF3P9cBISrS1rBQd0N1-y1hY84Za9gjQsPZ6KPaF1MVmrsDj6uT2880_EzG4UgN2oUMJcapQ9A-7Zzht4epZFsJXjmD7wfZbs2Va?width=297&height=660&cropmode=none" width="297" height="660" />

  <img src="https://dm2306files.storage.live.com/y4mgVLPXzZXI3h56XKRpcV50_tJizcpY0fb0LECmvkiAIQjn0XwOy0xbbExciMpYiK8KRpexsBHN6jdhG838UMgpHja9dL4zbX-MjOOnkXXGREGnewzWkFI6BJPwLID-j3ZmYWL7M-uDQBuEmpVT975SJJ2POxWWjhlgqqQLM2uymlC7pmnItNqFeWjeZWK5MJe?width=297&height=660&cropmode=none" width="297" height="660" />
</p>

