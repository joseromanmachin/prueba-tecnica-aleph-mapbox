package com.example.mapbox.ui.main.viewmodels

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mapbox.data.database.entitys.CoordenadaEntity
import com.example.mapbox.data.repository.MainRepository
import com.mapbox.geojson.Point
import kotlinx.coroutines.launch

class MainViewModel:ViewModel() {

    //service
    private lateinit var repository : MainRepository

    val listCoordenadas = MutableLiveData<ArrayList<CoordenadaEntity>>()


     fun onCreate(context : Context){
        repository = MainRepository(context)
    }

    fun insertCoordenada(point :Point){
        viewModelScope.launch {
            val coordenada = CoordenadaEntity(latitud = point.latitude(), longitud = point.longitude(), idCoordenada = null)
            repository.insertCoordenada(coordenada)
        }
    }


}