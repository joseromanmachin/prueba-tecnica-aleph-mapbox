package com.example.mapbox.ui.main.views.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mapbox.R
import com.example.mapbox.data.database.entitys.CoordenadaEntity
import com.example.mapbox.databinding.TarjetaCustomBinding
import com.example.mapbox.ui.main.views.MainActivityDetailView
import okhttp3.ResponseBody


class AdapterImagenes(
    private val dataSet: ArrayList<CoordenadaEntity>,
    private val style: String,
    private val resolucion: String,
    private val zoom: Double
) :RecyclerView.Adapter<AdapterImagenes.ViewHolder>(){

    lateinit var context : Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.tarjeta_custom, parent, false)
        context = parent.context
        return ViewHolder(view)
    }



    @SuppressLint("CheckResult")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = dataSet[position]

        (context as MainActivityDetailView).apply {
         obtenerImagen(style,"${data.longitud},${data.latitud}", zoom,resolucion).apply {

             holder.binding.textLatitudedeDetail.text = data.latitud.toString()
             holder.binding.textLogintudeDetail.text = data.longitud.toString()
             holder.binding.cardview.visibility = View.VISIBLE
          //   holder.binding.progressBar.visibility = View.GONE
             Glide.with(context).load(this)
                 .into(holder.binding.imageView)
         }

        }





    }



    override fun getItemCount(): Int = dataSet.size

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val binding = TarjetaCustomBinding.bind(view)
    }
}


