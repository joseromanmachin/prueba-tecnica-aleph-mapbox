package com.example.mapbox.ui.main.views

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mapbox.R
import com.example.mapbox.data.database.entitys.CoordenadaEntity
import com.example.mapbox.data.network.models.EstiloModel
import com.example.mapbox.databinding.ActivityMainDetailViewBinding
import com.example.mapbox.ui.main.viewmodels.MainDetailViewModel
import com.example.mapbox.ui.main.views.adapters.AdapterImagenes


class MainActivityDetailView : AppCompatActivity() {
    //binding
    private lateinit var binding: ActivityMainDetailViewBinding

    //viewmodel
    val viewModel: MainDetailViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inicializarComponentes()
        observers()

    }

    private fun observers() {
        viewModel.listCoordenadas.observe(this) {
            llenarRecyclerView(it)
        }
        viewModel.listEstilos.observe(this) {
            cargarSpinner(binding.spinnerSeleccioneEstilo, it)
        }
        viewModel.listResoluciones.observe(this) {
            cargarSpinner(binding.spinnerSeleccioneResolucion, it)
        }
        viewModel.cargando.observe(this) {
            cargando(it)
        }
        
        viewModel.mensajes.observe(this){
            mensaje(it)
        }



    }


    private fun llenarRecyclerView(lista: ArrayList<CoordenadaEntity>) {
        val estilo = binding.spinnerSeleccioneEstilo.selectedItem as EstiloModel
        val resolucion = binding.spinnerSeleccioneResolucion.selectedItem.toString()
        val zoom = binding.txtZoomInput.text.toString().toDouble()
        binding.recycler.hasFixedSize()
        binding.recycler.layoutManager = LinearLayoutManager(this)
        binding.recycler.adapter = AdapterImagenes(
            lista,
            estilo.id,
            resolucion, zoom
        )
    }

    private fun inicializarComponentes() {
        //binding
        binding = ActivityMainDetailViewBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel.onCreate(this)
        viewModel.getEstilos()
        viewModel.getResoluciones()

        binding.button.setOnClickListener {
            validarObtenerInformacion()
        }
        binding.floatingActionButton.setOnClickListener {
            eliminarPuntosGuardados()
        }
        validarObtenerInformacion()

    }



    private fun validarObtenerInformacion() {
        val zoom = binding.txtZoomInput.text
        if (zoom.isNullOrEmpty()) {
            binding.txtZoomInput.error = getString(R.string.campo_requerido)
        } else if (zoom.toString().toDouble() >= 23) {
            binding.txtZoomInput.error = getString(R.string.nivel_zoom)
        } else {
            viewModel.getAllCoordenadas()
        }
    }

    private fun cargarSpinner(spinner: Spinner, lista: ArrayList<*>) {

        val adapter = ArrayAdapter(
            applicationContext,
            android.R.layout.simple_spinner_dropdown_item,
            lista
        )

      //  spinner.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.adapter= adapter

    }

    fun obtenerImagen(
        style: String,
        coordenadas: String,
        zoom: Double,
        resolucion: String
    ): Bitmap {
        return viewModel.getImagen(style, coordenadas, zoom, resolucion)
    }


    private fun eliminarPuntosGuardados() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.confirmar_accion)

        builder.setPositiveButton(R.string.aceptar) { dialogInterface, i ->
            viewModel.deleteAllPuntos().let {
                 startActivity(Intent(this,MainActivityView::class.java))
                finish()
            }
        }
        builder.setNegativeButton(R.string.cancelar) { dialogInterface, i -> dialogInterface.dismiss() }

        val dialog: AlertDialog = builder.create()
        dialog.show()
    }



    private fun cargando(cargando: Boolean) {
        if (cargando){
            binding.progressBar.visibility = View.VISIBLE
            binding.button.visibility = View.GONE
        }else {
            binding.progressBar.visibility = View.GONE
            binding.button.visibility = View.VISIBLE
        }
    }

    private fun mensaje(mensaje : String ){
        Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show()
    }

}