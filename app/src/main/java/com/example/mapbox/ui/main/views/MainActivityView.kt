package com.example.mapbox.ui.main.views

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.example.mapbox.databinding.ActivityMainBinding
import com.example.mapbox.ui.main.viewmodels.MainViewModel
import com.example.mapbox.utils.Utils
import com.mapbox.geojson.Point
import com.mapbox.maps.Style
import com.mapbox.maps.plugin.gestures.addOnMapClickListener

class MainActivityView : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val viewModel:MainViewModel by viewModels()
    private val listPoints = ArrayList<Point>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        inicializarComponentes()
        configuracionMapview()
    }



    private fun inicializarComponentes() {
        //Binding
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel.onCreate(this)
        binding.button.setOnClickListener {
            segundaPantalla()
        }



    }



    private fun configuracionMapview() {

        binding.mapView.getMapboxMap().let {
            it.loadStyleUri(Style.MAPBOX_STREETS)
            it.addOnMapClickListener {point ->
                Utils.addAnnotationToMap(applicationContext,binding.mapView,point)
                guardarCoordenadas(point)
                binding.button.isEnabled = true
                true
            }
        }
    }

    private fun guardarCoordenadas(point: Point)  {
        viewModel.insertCoordenada(point)
    }


    private fun segundaPantalla() {
       startActivity(Intent(this,MainActivityDetailView::class.java))
        finish()
    }



    @SuppressLint("Lifecycle")
    override fun onStart() {
        super.onStart()
        binding.mapView.onStart()
    }

    @SuppressLint("Lifecycle")
    override fun onStop() {
        super.onStop()
        binding.mapView.onStop()
    }

    @SuppressLint("Lifecycle")
    override fun onLowMemory() {
        super.onLowMemory()
        binding.mapView.onLowMemory()
    }

    @SuppressLint("Lifecycle")
    override fun onDestroy() {
        super.onDestroy()
        binding.mapView.onDestroy()
    }


}