package com.example.mapbox.ui.main.viewmodels

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mapbox.data.database.entitys.CoordenadaEntity
import com.example.mapbox.data.network.models.EstiloModel
import com.example.mapbox.data.repository.MainRepository
import kotlinx.coroutines.*

class MainDetailViewModel : ViewModel() {



    //service
    private lateinit var repository: MainRepository

    val listCoordenadas = MutableLiveData<ArrayList<CoordenadaEntity>>()
    val listEstilos = MutableLiveData<ArrayList<EstiloModel>>()
    val listResoluciones = MutableLiveData<ArrayList<String>>()

    val mensajes = MutableLiveData<String>()
    val cargando = MutableLiveData<Boolean>()


    fun onCreate(context: Context) {
        repository = MainRepository(context)
    }


    fun getAllCoordenadas() {
        viewModelScope.launch {
            cargando.postValue(true)
            val response = repository.getAllCoordendas()
            if (response.size != 0) {
                listCoordenadas.postValue(response)
                cargando.postValue(false)
            }else {
                mensajes.postValue("sin datos !!")
            }
        }
    }

    fun getImagen(style:String,coordenadas:String, zoom: Double,resolucion: String ): Bitmap = runBlocking(Dispatchers.Unconfined) {
        val response = repository.getImagen(style, coordenadas, zoom, resolucion)
        val stream = response.byteStream()
        return@runBlocking BitmapFactory.decodeStream(stream)
    }

    fun getEstilos() {
       viewModelScope.launch {
           val response = repository.getEstilos()
           if (response.isSuccessful){
               listEstilos.postValue(response.body())
           }else {
               mensajes.postValue(response.message())
           }
       }
    }

    fun getResoluciones() {
        viewModelScope.launch {
            val response = repository.getResoluciones()
            if (response.isSuccessful){
                listResoluciones.postValue(response.body())
            }else {
                mensajes.postValue(response.message())
            }
        }

    }

    fun deleteAllPuntos() {
        cargando.postValue(true)

        viewModelScope.launch {
           val response  = repository.deleteAllCordenadas()
            cargando.postValue(false)
            mensajes.postValue(response)
        }
    }

}