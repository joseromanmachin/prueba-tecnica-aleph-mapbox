package com.example.mapbox.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.mapbox.data.database.daos.CoordenadasDao
import com.example.mapbox.data.database.entitys.CoordenadaEntity

@Database(entities = [CoordenadaEntity::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract val coordenadasDao:CoordenadasDao

    companion object{
        const val DATABASE_NAME = "db"
    }


    object db {
       fun getInstance(context: Context): AppDatabase {
           return Room
               .databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
               .allowMainThreadQueries()
               .build()
       }
   }
}