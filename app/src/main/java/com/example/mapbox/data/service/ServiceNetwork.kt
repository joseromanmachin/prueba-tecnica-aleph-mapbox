package com.example.mapbox.data.service

import android.content.Context
import com.example.mapbox.R
import com.kotlin.calculatutaxi.core.RetrofitInstance
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import com.example.mapbox.data.network.ApiClient
import com.example.mapbox.data.network.models.EstiloModel
import okhttp3.ResponseBody
import retrofit2.Response

class ServiceNetwork(val context : Context) {
    private val retrofit = RetrofitInstance.getRetrofit().create(ApiClient::class.java)

    suspend fun getImagen(style:String ,coordenada: String,zoom: Double, resolucion: String ): ResponseBody {
        return withContext(Dispatchers.IO){
            val response = retrofit.getImagen(style,coordenada, zoom,resolucion,context.getString(R.string.mapbox_access_token))
               response
        }

    }

   suspend fun getEstilos(): Response<ArrayList<EstiloModel>>{
        return withContext(Dispatchers.IO){
            val response = ArrayList<EstiloModel>()
            response.add(EstiloModel("dark-v10","Mapbox Dark"))
            response.add(EstiloModel("light-v10","Mapbox Light"))
            response.add(EstiloModel("streets-v11","Mapbox Streets"))
            Response.success(200,response)
        }
    }

    suspend fun getResoluciones(): Response<ArrayList<String>> {
        return withContext(Dispatchers.IO){
            val response = ArrayList<String>()
            response.add("600x200")
            response.add("400x200")
            response.add("200x200")
            Response.success(200,response)
        }
    }


}