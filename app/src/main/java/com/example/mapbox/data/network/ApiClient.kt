package com.example.mapbox.data.network

import okhttp3.ResponseBody
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface ApiClient {

    //Fotos
    @GET("{style}/static/{coordenadas},{zoom}/{resolucion}")
    suspend fun getImagen(
        @Path("style")style:String,
        @Path("coordenadas") coordenadas :String,
        @Path("zoom")zoom :Double,
        @Path("resolucion")resolucion :String,
        @Query("access_token")token :String): ResponseBody



}