package com.example.mapbox.data.database.daos

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.mapbox.data.database.entitys.CoordenadaEntity

@Dao
interface CoordenadasDao {
    @Query("select * from coordenadas")
     fun getAll(): List<CoordenadaEntity>

    @Insert
     fun insert(carga: CoordenadaEntity)

    @Delete
     fun delete(carga: CoordenadaEntity)

     @Query("delete from coordenadas")
    fun deleteAll()
}