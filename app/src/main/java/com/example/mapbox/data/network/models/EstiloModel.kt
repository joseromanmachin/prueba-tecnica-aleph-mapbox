package com.example.mapbox.data.network.models

data class EstiloModel(val id:String , val Descripcion :String){
    override fun toString(): String {
        return this.Descripcion
    }
}
