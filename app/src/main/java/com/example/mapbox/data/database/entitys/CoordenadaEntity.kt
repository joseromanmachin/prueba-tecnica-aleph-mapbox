package com.example.mapbox.data.database.entitys

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "coordenadas")
data class CoordenadaEntity(
    @PrimaryKey(autoGenerate = true) val idCoordenada: Int?,
    val latitud:Double,
    val longitud:Double)


