package com.example.mapbox.data.repository

import android.content.Context
import com.example.mapbox.data.database.entitys.CoordenadaEntity
import com.example.mapbox.data.network.models.EstiloModel
import com.example.mapbox.data.service.ServiceDataBase
import com.example.mapbox.data.service.ServiceNetwork
import okhttp3.ResponseBody
import retrofit2.Response

class MainRepository(context: Context) {

    private val service = ServiceNetwork(context)
    private var serviceDataBase: ServiceDataBase = ServiceDataBase(context)


    suspend fun getAllCoordendas(): ArrayList<CoordenadaEntity> {
           return serviceDataBase.getCoordenadas() as ArrayList<CoordenadaEntity>
    }


  suspend  fun insertCoordenada (coordenadaEntity: CoordenadaEntity){
        serviceDataBase.insertCoordenada(coordenadaEntity)
    }

    suspend fun getImagen(style:String,coordenadas:String,zoom: Double, resolucion: String ): ResponseBody {

        return service.getImagen(style,coordenadas,zoom,resolucion)
    }

    suspend fun getEstilos(): Response<ArrayList<EstiloModel>> {
        return service.getEstilos()
    }

    suspend fun getResoluciones(): Response<ArrayList<String>> {
        return service.getResoluciones()
    }

    suspend fun deleteAllCordenadas() : String{
        return serviceDataBase.deleteCoordenadas()
    }


}
