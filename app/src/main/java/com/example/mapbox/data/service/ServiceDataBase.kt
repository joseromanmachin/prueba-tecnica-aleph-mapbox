package com.example.mapbox.data.service

import android.content.Context
import com.example.mapbox.data.database.entitys.CoordenadaEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import com.example.mapbox.data.database.AppDatabase

class ServiceDataBase(context: Context) {
    private var database: AppDatabase = AppDatabase.db.getInstance(context)

    suspend fun getCoordenadas():List<CoordenadaEntity> {
        return withContext(Dispatchers.IO) {
            val response = database.coordenadasDao.getAll()
            response
        }
    }


    suspend fun deleteCoordenada(coordenada: CoordenadaEntity){
        return withContext(Dispatchers.IO){
            database.coordenadasDao.delete(coordenada)
        }
    }




  suspend  fun insertCoordenada(coordenadaEntity: CoordenadaEntity) {
        withContext(Dispatchers.IO){
            database.coordenadasDao.insert(coordenadaEntity)
        }
    }

    suspend fun deleteCoordenadas() :String{
        withContext(Dispatchers.IO){
            database.coordenadasDao.deleteAll()
        }
        return "db eliminada"

    }

}


